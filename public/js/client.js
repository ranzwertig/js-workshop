(function() {

  ($(document)).ready(function() {
    var socket;
    socket = io.connect('http://192.168.35.171:8080/');
    socket.on('connect', function() {
      return console.log('socket connected');
    });
    socket.on('disconnect', function() {
      return console.log('socket disconnected');
    });
    socket.on('data:' + ($('#tag')).val(), function(data) {
      return ($('#messages')).append("<li>" + data.name + ": " + data.message + "</li>");
    });
    return ($('#sendio')).click(function(e) {
      socket.emit('data', {
        message: ($('#message')).val(),
        name: ($('#name')).val(),
        tag: ($('#tag')).val()
      });
      return ($('#messages')).append("<li>" + (($('#name')).val()) + ": " + (($('#message')).val()) + "</li>");
    });
  });

}).call(this);
