###
    Module Pattern in Coffee

    The module pattern works with coffee too:
###

MODULE = ->
    me = 

    me.greet = ->
        console.log hello
()

###
    * you should compile coffee with -b (--bare) option, so you 
        don't end up with that

(function() {
  var MODULE;

  MODULE = (function() {
    var me;
    return me = me.greet = function() {
      return console.log(hello);
    };
  })();

}).call(this);
###