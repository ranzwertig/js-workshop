/*
    The Module Pattern

    * a module is nothing more than basic JS pattern
    * all functions and vars are kept in the modules scope

    * the module pattern basically is a function which gets
        called immediately (immediate invocation)
*/

(function () {
    // modules functions and variables
}());