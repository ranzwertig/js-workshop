/*
    Module Export

    to make the functions in our module accessible by others we could

    * bind them to global vars (not that good :))

    * export them to the modules own namespace 

    ==> this basically is the complete module pattern
*/

var MODULE = (function(){

    var me = {};

    var privateMethod = function(){
        // can't be called from outside the module
    };

    me.greet = function(){
        console.log('hello');
    };

    return me;

})();

// now we can access the greet function with 

MODULE.greet();