/*
    Augmentation

    * the module patter has the limitation that 
        everything has to be in one file

    * the module itself is injected into the module ?!

    * so we can have the module spread over more than one file
        ==> quite useful for bigger projects

    * but we cant share private state between them!
*/

// tight augmentation
var MODULE = (function(me){

    var privateMethod = function(){
        // can't be called from outside the module
    };

    me.greet = function(){
        console.log('hello');
    };

    return me;

})(MODULE);

// loose augmentation
var MODULE = (function(me){

    var privateMethod = function(){
        // can't be called from outside the module
    };

    me.greet = function(){
        console.log('hello');
    };

    return me;

// if there is no MODULE hust use {}
})(MODULE || {});