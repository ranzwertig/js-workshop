/*
    Use Globals 

    * we have to use globals if we use e.g. jQUery
    * we can give the global our own name inside the module
    * you can't see which globals are present in global scope so

        ==> just use globals you inject into the module
*/

(function($, whateverGlobal){
    // ...
})(jQuery, YUI);