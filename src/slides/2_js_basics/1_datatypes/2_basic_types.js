#!/usr/bin/env node
/*
    Booleans, null and, undefined

    * within these datatypes there are four constants:
        * true
        * false
        * null
        * undefined

    * typeof SOMETIMES gives you the type 

*/

console.log(typeof true);
console.log(typeof false);
console.log(typeof null); // WTF?!
console.log(typeof someUndefinedVar);