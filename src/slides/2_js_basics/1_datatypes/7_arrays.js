#!/usr/bin/env node
/*
    Array

    * Arrays are Objects! so typeof is not that helpful

*/

console.log(typeof []);

// iterating with for ... in
var myArray = ['John', 'Marry', 'Horst'];

for (var key in myArray) {
    console.log(myArray[key]);
}

for (var i = 0; i < myArray.length; i++)
    myArray[i]