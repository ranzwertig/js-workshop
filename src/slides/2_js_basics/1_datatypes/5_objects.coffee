#!/usr/bin/env coffee
###
    Creating Objects in coffee

    * the nottion for object literals is = (/n [tab] property: value)*
    * which i think is quite ugly when you are passing objects to functions
###

myEmptyObject =

mySecondObject = new Object()

myObject = 
    name: 'Horst Brack'
    sayHi: ->
        console.log 'hi'

myObject.sayHi()


f = (args...) ->
    console.log args

# whats that?
f 
    name: 'Horst'
,
    name: 'John'

# this sucks huh??!!
# I'll prefer:

f {name: 'Horst'}, {name: 'John'}