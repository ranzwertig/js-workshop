#!/usr/bin/env node
/*
    Numbers
    
    In JS all numbers are represented by Number datatype

    * double-precision 64-bit binary format IEEE 754 value
    * no Integers
    * There are some symbolic values in number
        * Infinity
        * -Infinity
        * NaN

    * there are Binary operators 
        -> https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Operators/Bitwise_Operators
*/

// How can we write this shorter ?
console.log([Infinity, -Infinity]);

console.log(parseInt('aosidhoasidhf', 10));

