#!/usr/bin/env node
/*
    Iterating Objects

    * mostly we just like to iterate direct properties
    * for ... in ... iterates the prototype too
    * prototype ?! --> later
*/
var o = new Object();
o.age = 29;

// add a property to the objects prototype
o.__proto__.name = 'foo';


for (var key in o) {
    console.log(key);
}

for (var key in o) {
    if (o.hasOwnProperty(key)) {
        console.log(key);
    }   
}