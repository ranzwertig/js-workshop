#!/usr/bin/env node
/*
    Objects

    * almost everithing in js is a object
    * Objects contain properties
        * properties can be added and removed
    * we mostly use literals to create objects
    * Objects can contain everything!
    * the keys are represented by strings
*/

var myEmptyObject = {};

var mySecondObject = new Object();

var myObject = {
    1: 'Horst Brack',
    sayHi: function(){
        console.log('hi')
    }
};
myObject.sayHi();

myObject =
    1: 'Horst'
    sayHi: ->
        console.log 'hi'