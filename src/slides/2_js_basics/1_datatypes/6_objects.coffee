#!/usr/bin/env coffee
###
    Iterating Objects

    * mostly we just like to iterate direct properties
    * for ... of ... iterates the prototype too
    * prototype ?! --> later
    * in coffee user FOR ... OF ... for iterating objects!
###
o = new Object()

o =

o.age = 29
o.__proto__.name = 'foo'

for key of o
    console.log key

for own key of o
    console.log key

# get the values too:
for own key, value of o
    console.log key
    console.log value