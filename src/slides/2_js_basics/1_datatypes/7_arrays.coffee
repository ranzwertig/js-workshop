#!/usr/bin/env coffee
###
    Arrays
    * in coffee we can use the js common array literal
    * there is a comfortable way to destruct arrays in coffee
###

myArray = []
myArray.push 1

aArray = [1, 2, 3, 4, 5, 6, 7, 8, 9]

person = ['John','Doe', 23]

[firstname, lastname, age] = person
console.log firstname
console.log lastname
console.log age

# we also can do
[first, middle..., last] = aArray

###
    iterating arrays with for ... in ...
###
people = ['John', 'Marry', 'Justin', 'Horst']
for person in people
    console.log person

###
    we can generate array with [...] or [..] (python like)
###
console.log [1..10]
console.log [1...10]

###
    or get just some parts of an array
###
console.log aArray[1...3]
console.log aArray[1..3]
console.log aArray[1..]