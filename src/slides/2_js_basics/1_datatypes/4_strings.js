#!/usr/bin/env node
/*
    Strings

    * JavaScript strings are immutable
        After creation they can't be changed
    * Operations you apply to Strings mostly generate a new String
    * concatenation operator for strings is + 
*/

console.log(typeof 'hello');
console.log(typeof new String('hello'));

/*
    concatenation using + is quite slow, becaus each time we use
    + a new string is created.

    * depends on js engine!
*/

// concat using +
var start = new Date().getTime();
var string = '';
for(var i = 0; i < 1000000; i++) {
    string += 'hello';
}
var end = new Date().getTime();
var time = end - start;
console.log('Execution time +: ' + time);

// now use [].join()
var start = new Date().getTime();
var strings = [];
for(var i = 0; i < 1000000; i++) {
    strings.push('hello');
}
strings.join('');
var end = new Date().getTime();
var time = end - start;
console.log('Execution time array: ' + time);