#!/usr/bin/env node
/*
    the arguments variable in a function scope gives a array-like object containing 
    all arguments the function was called with
*/
var someFunction = function(a,b,c) {
    console.log(arguments);
};

someFunction(1, 2, 3);
someFunction(1, 2);
someFunction(1);