#!/usr/bin/env coffee
###
    in coffe we define functions by -> operator

    * there is a other operator => which I'll introduce later
###

# function without a parameter
aFunction = ->

console.log aFunction

# function with parameter
bFunction = (a) ->
    console.log a

bFunction(1)