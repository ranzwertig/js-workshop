#!/usr/bin/env coffee
###
    access the argumants array

    * quite useful for factories
###
doSth = (first, args...) ->
    console.log first
    console.log args

doSth 1,2,3,4,5

###
    we can use apply in coffee too

    * there is the common .apply notation
    * ore the ... notation
###

someArray = [1, 2, 3, 4, 5, 6]

doSth.apply null, someArray
# same as
doSth someArray...