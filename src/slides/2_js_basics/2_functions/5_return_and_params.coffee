#!/usr/bin/env coffee
###
    in coffee the last statement in a function will be returned
###
returnsSth = ->
    "Hello World"
    # return "Hello World"
    # * would do the same job

console.log returnsSth()

###
    there are optional parameters in coffee

    * the optional params have to be at the end of the param list
###
sayHi = (to = 'John Doe', from = 'Chris') ->
    "Hi #{to}, I am #{from}"

console.log sayHi 'CommPress'
console.log sayHi 'CommPress', 'JS'
console.log sayHi()