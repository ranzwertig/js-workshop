#!/usr/bin/env node
var aFunction = function(a) {
    console.log(this);
    console.log(a);
};

/*
    Functions are objects! First class objects.

    * We can call functions by calling the .call method of a funtion.
    * We can set the value of this in the function scope.
*/
aFunction.call({}, 3);

/*
    Or we can use the apply method which uses an array to pass
    the parameters
*/
aFunction.apply({}, [3]);