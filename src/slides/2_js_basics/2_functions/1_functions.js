#!/usr/bin/env node
/*
    we can just define a anonymous function which wille be
    bound to a variable;
*/

var aFunction = function(a) {
    console.log(this);
    console.log(a);
};

console.log(aFunction);
// [Function]

/*
    don't use global function"
*/
function a(a) {
    console.log(a);
}

a(1);
aFunction(1);