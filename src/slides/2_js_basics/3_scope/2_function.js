#!/usr/bin/env node
/*
    If variables are defined inside a function, they are in 

    * function scope
    * they are boxed in by the function

    * we can't access l from outside the function
*/

var f = function () {
    var l = 'local';
};
f();

console.log(l);