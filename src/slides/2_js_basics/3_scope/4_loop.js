#!/usr/bin/env node
/*
    so what the hell happened here?

    * i is visible in the whole f1 function scope
    * that means its visible in the closures scope too
    * at the time when items[1]() or items[X]() is called i has the value 10
    * and all generated functions share the SAME variable i
*/

var f11 = function() {
    var message = 'Hello Dudes';
    var fInner = function(){
        console.log(message);
    };
    var message = 'Talking Rubbish!';
    fInner();
};
f11();

/*
    * => NEVER DEFINE CLOSURES IN LOOPS!!!
*/