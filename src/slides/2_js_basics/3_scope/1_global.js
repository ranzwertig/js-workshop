#!/usr/bin/env node
/*
    There are global variables in JS

    whenever you do sth like ...
    ... it is global an can be acessed from everywhere in your module
*/

var globalVar = 'global';

// DO NOT USE GLOBAL VARIABLES !!!