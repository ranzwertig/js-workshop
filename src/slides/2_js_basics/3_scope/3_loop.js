#!/usr/bin/env node
/*
    There are some function scope concerning thing which
    can drive you crazy:

    WHAT HAPPENS HERE?
*/

var f1 = function() {
    var items = {};
    for (var i = 0; i < 10; i++ ) {
        items[i] = function(){
            console.log(i);
        };
    }
    items[1](); // ?
    items[5](); // ?
};
f1();