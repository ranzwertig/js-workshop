#!/usr/bin/env node
/*
    BETTER:
*/

var f2 = function() {
    var items = {};

    var functionFactory = function(a){
        return function(){
            console.log(a);
        };
    };

    for (var i = 0; i < 10; i++ ) {
        items[i] = functionFactory(i);
    }

    items[1]();
    items[5]();
};
f2();