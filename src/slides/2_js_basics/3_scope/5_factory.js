#!/usr/bin/env node
/*
    Function factories do the job

    * ????
    * freakin ugly when they are inlined!
*/

var f2 = function() {
    var items = {};

    for (var i = 0; i < 10; i++ ) {
        items[i] = (function(a){
            return function(){
                console.log(a);    
            }
        })(i);
    }
    items[1]();
    items[5]();
};
f2();