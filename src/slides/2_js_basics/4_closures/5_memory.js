#!/usr/bin/env node
/*
    so this is the solution !

    * all 4 variables will be shared on each function call
    * they exists just once in memory
    * they are NOT global accessible
*/


var someFunction = (function(){

    // variables are in function scope
    // so they are NOT global!
    var TYPE_1 = 1;
    var TYPE_2 = 10;
    var SOME_VALUE = 1;
    var theAnswer = 42;

    // closure is returned and uses vars from outher scope
    return function(x){
        if (x < TYPE_2) {
            return false;
        } else if (x >= TYPE_1) {
            return x * SOME_VALUE === theAnswer;
        }
    };
})();

console.log(someFunction(1));
console.log(someFunction(2));
console.log(someFunction(42));