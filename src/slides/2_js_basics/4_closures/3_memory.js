#!/usr/bin/env node
/*
    Sometimes we need some global static values in funftions   

    * whenever we call someFunction the 4 variables are created in memory
    * they so to be quite constant so probably they can be shared? 
*/

var someFunction = function(x){
    var TYPE_1 = 1;
    var TYPE_2 = 10;
    var SOME_VALUE = 1;
    var theAnswer = 42;

    if (x < TYPE_2) {
        return false;
    } else if (x >= TYPE_1) {
        return x * SOME_VALUE === theAnswer;
    }
};

console.log(someFunction(1));
console.log(someFunction(2));
console.log(someFunction(42));
