#!/usr/bin/env node
/*
    Closures

    * remembers its context from creation time
    * a function becomes a closure whenever it returned from another
        function
*/

var functionFactory = function(){
    var someVariable = 'var';
    // return the closure
    return function(){
        // do sth woth someVariable
        return someVariable;
    };
};

console.log(functionFactory());
console.log(functionFactory()());