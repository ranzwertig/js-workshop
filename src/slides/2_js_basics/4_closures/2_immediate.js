#!/usr/bin/env node
/*
    Use immediate invocation for the factory

    * the closure is returned immediately and referenced by fn
*/
var fn = (function(){
    var someVariable = 0;
    // return the closure
    return function(){
        // do sth woth someVariable
        return ++someVariable;
    };
})();

console.log(fn());
console.log(fn());
console.log(fn());
console.log(fn());