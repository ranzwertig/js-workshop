###
    Custom Event Emitter

    * mostly in node
###

class MyEmittingClass extends EventEmitter

    cunstructor: (@name) ->
        setTimeout =>
            @emit 'constructed', @name
        , 2000

emitter = new MyEmittingClass 'spock'

emitter.addListener 'constructed', (name) ->
    console.log "#{name} got constructed 2 sec before"