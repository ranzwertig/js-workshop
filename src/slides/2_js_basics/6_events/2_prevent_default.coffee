###
    Prevent Default

    * if event.preventDefault() is called on a event the deafult action
        is not performed

    * see e.g. https://developer.mozilla.org/samples/domref/dispatchEvent.html
###

($ '#myButton').on 'click', (e) ->
    e.preventDefault()