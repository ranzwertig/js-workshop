/*
    Events

    * in browser environments events are used to listen for..

        * user interactions
        * messaging (sockets, web workers, ...)

    * to listen on events we bind handlers to the event

    * in Browsers we use addEventListener
*/

myButton.addEventListener('click', function(){...}, true);

// the third param is called 'useCapture' it defines if the listner
// should listen for capture or bubble of the event