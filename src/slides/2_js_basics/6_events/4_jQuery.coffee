###
    jQuery Events

    * whenever we return false in a jQuery event it's same as
        calling

        e.preventDefault()
        e.stopPropagation()
    
###

($ '#myButton').on 'click', (e) ->
    alert 'hello'
    false

# same as

($ '#myButton').on 'click', (e) ->
    alert 'hello'
    e.preventDefault()
    e.stopPropagation()
