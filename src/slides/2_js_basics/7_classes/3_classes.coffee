###
    Classes

    * coffee gives us a more abstract an well known layer for classes
    * you can give a shit about prototypal inheritance
###

class Animal

    ###
        @ mens this
        When we declare parameters in the constructor with @ they are
        automatically bound to this.
    ###
    constructor: (@name) ->

    makeNoise: (noise='') ->
        if noise is ''
            console.log "#{@name} says #{@noise}"
        else
            console.log "#{@name} says #{noise}"

###
    it's quite easy to extend classes in coffee
###
class Dog extends Animal

    makeNoise: (noise) ->
        super(noise)


dog = new Dog 'snoopy', 'wuff'

dog.makeNoise()
dog.makeNoise('rrrrrrr')
