/*
    Classes

    in Javascript classes follow a different pradigm than e.g. in php

    * classes are built by their constructor functions
    * inside this function we can add attributes by binding
        them to this 
    * we can define functions inside the constructor function
        ==> but they won't be sared between all instances
    * we can share functions over all objects of a class by binding them 
        to the prototype
    
*/

var Animal = function(name, noise){
    this.name = name;
    this.noise = noise;

    /*
    this.makeNoise = function(){
        console.log("" + this.name + " says " + this.noise);
    };

    we could do this too but wastes some memory
    */
};

/*
functions defined in the prototype are shared functions
*/
Animal.prototype.makeNoise = function(){
    console.log("" + this.name + " says " + this.noise);
};

var dog = new Animal('snopy', 'bark');
dog.makeNoise();