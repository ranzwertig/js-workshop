###
    Function binding

    * in coffee the => operator solves the problem
    * it binds the actual ment this to the function

###

class Tools

    constructor: (@instanceName) ->

    process: =>
        console.log @instanceName



tools = new Tools 'bla'

[1,2,3,4,5,6].forEach tools.process