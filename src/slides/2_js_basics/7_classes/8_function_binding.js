/*
    Function binding

    * in JS this would look like ...

*/

var Tools,
    // the methof which does the magic
  __bind = function(fn, me){ 
    // this becomes me
    // return the function which uses apply to bass the richt this (now me) to the function
    return function(){ 
      return fn.apply(me, arguments); 
    }; 
  };

Tools = (function() {
  function Tools(instanceName) {
    this.instanceName = instanceName;
    // overwrite the process function so it is never called from constructor
    this.process = __bind(this.process, this);
  }

  // the actual function
  Tools.prototype.process = function() {
    return console.log(this.instanceName);
  };

  return Tools;

})();