###
    Function binding

    * in JS this is dynamically scoped
    * so it references always the object the function is currently bound to
    * by passing a function as a callback or attaching it to a different object
        the original this will get lost

###

class Tools

    constructor: (@instanceName) ->

    process: ->
        console.log @instanceName



tools = new Tools 'bla'

[1,2,3,4,5,6].forEach tools.process