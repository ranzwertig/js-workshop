/*
    Prototype

    * the prototype of a class will be shared over all objects
    * by acessing a property js looks it up in the direct properties
        first
    * if it can't be found there js looks it up in the prototype
    * we can access a objects prototype using the object.__proto__
        object (non standard)

    * Inheritance can be done by binding a new object of the parent class
        to the child classs prototype
*/

function A(a){

    var x = 0;
    this.y = 0;

    this.varA = a;

    this.getX = function(){
        return x;
    }
}
 
A.prototype = {
    varA : null,
    doSomething : function(){
        // ...
    }
};

function B(a, b){
    A.call(this, a);
    this.varB = b;
}

B.prototype = Object.create(A.prototype, {
    doSomething : function(){ // override
        A.prototype.doSomething.apply(this, arguments); // call super
        // ...
    },
});

/*
    Just use Object.create with polyfill
*/

if (!Object.create) {
    Object.create = function (o) {
        if (arguments.length > 1) {
            throw new Error('Object.create implementation only accepts the first parameter.');
        }
        function F() {}
        F.prototype = o;
        return new F();
    };
}
// by https://developer.mozilla.org/de/docs/JavaScript/Reference/Global_Objects/Object/create