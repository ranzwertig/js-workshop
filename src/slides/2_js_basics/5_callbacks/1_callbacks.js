/*
    Callbacks

    * quite important in JavaScript
    * used for event handlers, I/O, ...
    * makes asynchronous I/O possible in async environments
    --> JavaScript is NOT async jut the environment it's running in is!
    
*/

var myLongRunningFunction = function(callback) {
    // this will take hours and i don't ike to wait for it

    // whenever its ready call the callback
    callback();
};

// as you see a callback function is passed as agrument.
myLongRunningFunction(function(){
    console.log('DONE!');
});