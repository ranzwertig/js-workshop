/*
    Callbacks in loops can be a bitch!

    Why:

    * a callback can be called more than once
    * in bigger functions it makes things easier if we overwrite the callback after it's called
    * if you never miss a return statement you don't need this :)

*/

var loppsSthGetFirst = function(anArray, cb){

    for (num in anArray) {
        if (anArray[num] === 42) {
            cb(num);
            cb = function(){};
        }
    }
};

loppsSthGetFirst([1,42,3,4,42], function(pos){
    console.log('got first at pos' + pos);
});