/*
    There are best practices for callbacks

    * if there could be an error the first argument returned is the error
    * arguments after the error return the actual data

*/

var is42elseError = function(x, cb){

    if (x === 42) {
        // call the callback with an error != null
        cb(new Error('x is not 42'));
        return;
    }
    // call the callback without error but it returns the actual data
    cb(null, true);
};

is42elseError(1, function(error, is42) {
    if(error) {
        console.log(error);
        return;
    }
    
    if (is42) {
        console.log('the answer!');
    } else {
        console.log('doh!');
    }
});