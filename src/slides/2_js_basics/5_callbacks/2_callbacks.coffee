###
    In coffee callbacks work the dame way
###

myLongRunningFunction = (callback) ->
    # this will take hours and i don't ike to wait for it

    # whenever its ready call the callback
    callback()

# as you see a callback function is passed as agrument.
myLongRunningFunction ->
    console.log 'DONE!'