###
    Callbacks in a async environment

    * async environments are normally single threaded
    * they use a eventloop
    * there is just one function running at the same time
    * in Browser environments the eventloop is mostly idle
        unless there is a event or a request
###

# e.g read a file in nodejs

fs = require 'fs'

fs.readFile 'path/to/file.txt', (error, data) ->
    if error
        console.log eror
    else
        console.log data