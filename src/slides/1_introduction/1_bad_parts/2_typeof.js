/*
    Typeof 

    * returns the datatype
    * in JS almost everything is a Object
*/

console.log(typeof []);

console.log(typeof new Object());

console.log(typeof null);

console.log(typeof new String('hello'));

console.log(typeof 'hello');