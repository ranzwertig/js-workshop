/*
    Other bad Parts

    * semicolon insertion
        * if you forget a ; JS just tries to add one.
        * SO: ALWAYS add semicolons!

    * with
        * just DON'T use it!

    * eval
        * NEVER EVER USE IT!!

    * phony arrays
        * in js arrays are objects
        * they are technical hash maps
        * keys are strings and get hashed
        * items not ordered in memory
        * makes the language much easier
*/