/*
    Equality

    * JavaScript has two different types of testing eqiality

        * == (!=)
        * === (!==)

    * == does type coercion
    * === not
*/

console.log("\n'' == '0'");
console.log('' == '0');

console.log("\n0 == ''");
console.log(0 == '');

console.log("\n0 == '0'");
console.log(0 == '0');

console.log("\nfalse == 'false'");
console.log(false == 'false');

console.log("\nfalse == '0'");
console.log(false == '0');

console.log("\nfalse == undefined");
console.log(false == undefined);

console.log("\nfalse == null");
console.log(false == null);

console.log("\nnull == undefined");
console.log(null == undefined);

console.log("\n' \\t\\r\\n' == 0");
console.log(' \t\r\n' == 0);

console.log("\nNaN == NaN");
console.log(NaN == NaN);