/*
    globals Variables

    * in JS we can define global variables.
    * this can lead to a strange beaviour in your programm
    * NEVER USE GLOBALS!
*/

// e.g. in /js/mymodule/anything.js
var i = 0;

$('#addToChart').click(function(){
    i++;
});

// e.g. in /js/app/myall.js

for (i = 0; i < 10; i++) {
    // do sth
}
