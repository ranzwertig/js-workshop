/*
    Concatenation

    * JS uses + for additions and concatenations
    * normally no problem if the language isn't loosely typed
    * the bad thind is + does type coercion
*/

console.log(7 + 7 + 7);

console.log(7 + 7 + '7');

console.log('7' + 7 + 7);