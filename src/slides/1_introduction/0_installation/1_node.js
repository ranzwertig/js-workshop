/*
    First of all: Node.js

    * we have to install node to run js, run tests, use build scripts, ...
    * it a quite useful tool even if you do not develop for node

*/

/*
    Installation

    1. goto http://nodejs.org/download/
        there you'll get binaries for almost everything you need (npm included)

    2. install via package manager
        https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager

*/