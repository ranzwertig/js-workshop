###
    Spies

    * decoupled testing
    * faster testes (no more timeouts)
    * async testing

###

f = (func) ->
    setTimeout func, 500

describe 'timedFunction', ->

    it 'should call the callback', ->
        callback = createSpy 'callback'

        (spyOn global, 'setTimeout').andCallFake (func) ->
            func()

        f callback
        
        (expect callback).toHaveBeenCalled()