###
    Rewire

    * dependency injection for JS
    * https://github.com/jhnns/rewire

###

http = require 'http'

ddos = (host) ->
    for i in [0..100000]
        http.get host

module.exports = ddos;

# in the tests

ddos = rewire '../lib/ddos.js'

ddos.__set__ 'http', httpMock