###
    Provide a API for mocking

    * you have to change the modules for testing
###


ddos.http = require 'http'

ddos = (host) ->
    for i in [0..100000]
        ddos.http.get host

module.exports = ddos;

# in the test do

ddos = require '../lib/ddos.js'

ddos.http = httpMock