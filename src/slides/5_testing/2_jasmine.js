/*
    Jasmine

    * almost standard for JS
    * works with nodejs and browsers (server + client)
    * Spies
    * better errors with custom matchers
        https://github.com/pivotal/jasmine/wiki/Matchers#writing-new-matchers

*/

beforeEach(function() {
  this.addMatchers({

    toBeLessThan: function(expected) {
      var actual = this.actual;
      var notText = this.isNot ? " not" : "";

      this.message = function () {
        return "Expected " + actual + notText + " to be less than " + expected;
      };

      return actual < expected;
    }

  });
});