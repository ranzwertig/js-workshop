###
    Or we can do monkey patching
    
    * replace the actual get method with the mock
    * don't forget to revert

###

http = require 'http'

ddos = (host) ->
    for i in [0..100000]
        http.get host

module.exports = ddos;

# in the test do

http = require 'http'
originalGet = http.get;

ddos = require '../lib/ddos.js'
# ...

http.get = getMock
http.get = originalGet
