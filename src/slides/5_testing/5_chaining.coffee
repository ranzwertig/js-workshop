###
    Chain Matchers

    * jasmine uses chaning for negation
    * bot not too much chaning

        should =>  array.should.not.contain(1)
###

it 'should contain `the answer`', ->
    (expect 'some random text').toContain('the answer')

# Expected 'some random text' to contain 'the answer'

it 'should contain `the answer`', ->
    (expect 'some random text').not.toContain('the answer')