###
    Custom Matchers

    * the default matchers often give undefined and
        ununderstandable errors
    * you can get better errors with custom matchers
###

it 'should be instance of function', ->
    (expect 1 instanceof Function).toBe true

# Expected true to be false 

it 'should be instance of function', ->
    (expect 1).toBeInstanceOf Function

# Expected 1 to be instance of Function

beforeEach ->
    @addMatchers
        toBeInstanceOf: (fConstructor) ->
            return @actual instanceof fConstructor