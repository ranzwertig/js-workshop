/*
    Sometimes we like to do stuff in parallel

    the problem here:

    * at the point of res.send(fileinfo); the whole fileinfo wasn't fetched yet
    * we have to wait for the loop to be finished.
*/

app.get('/fileinfos', function(req, res){
    var files = ['/file1.txt', 'file2.txt', 'file3.txt'];
    var fileinfo = '';
    files.each(function(){
        fs.getFileInfo(this, function(info){
            fileinfo = fileinfo + ' ' + info;
        });
    });
    res.send(fileinfo);
});