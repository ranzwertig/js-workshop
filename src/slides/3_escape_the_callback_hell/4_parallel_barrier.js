/*
    The example before using a barrier
*/

app.get('/fileinfos', function(req, res){
    var files = ['/file1.txt', 'file2.txt', 'file3.txt'];
    var fileinfo = '';
    
    var barrier = new Barrier(files.length, function(){
        res.send(fileinfo);
    }, function(){});
    
    files.each(function(){
        fs.getFileInfo(this, function(info){
            fileinfo = fileinfo + ' ' + info;
            barrier.submit();
        });
    });
});