/*
    The Hell

    * in JS a lot of code looks like this

*/

getConfiguration(appId, function(err, config){
    getDbConnection(config, function(err, db){
        db.getInfo('SELECT servername FROM servers', function(err, name){
            startServer(name, function(app){
                app.get('/data', function(req, res) {
                    db.query('SELECT * FROM data', function(err, data){
                        data.each(function(value){
                            res.send(value);
                        });
                    });
                });
            });
        });
    });
});

/*
    Solution

    * modules
    * oop
    * divide into more functions
    * put logic in a module and environment related stuff in a different
*/