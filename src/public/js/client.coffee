($ document).ready ->

    # init socket io
    socket = io.connect 'http://192.168.35.171:8080/'

    socket.on 'connect', () ->
        console.log 'socket connected'

    socket.on 'disconnect', () ->
        console.log 'socket disconnected'

    socket.on 'data:' + ($ '#tag').val(), (data) ->
        ($ '#messages').append "<li>#{data.name}: #{data.message}</li>"

    ($ '#sendio').click (e) ->
        socket.emit 'data',
            message: ($ '#message').val()
            name: ($ '#name').val()
            tag: ($ '#tag').val()
        ($ '#messages').append "<li>#{($ '#name').val()}: #{($ '#message').val()}</li>"