program = require 'commander'
io = require 'socket.io'
express = require 'express'
app = express()
server = (require 'http').createServer app
consolidate = require 'consolidate'

program
.option('-p, --port <n>', 'The port the programm is listening to', parseInt, 8080)
.option('-a, --address <n>', 'The Ip listenign to', '192.168.35.171')
.parse(process.argv)

# http://pastebin.com/yC5X1tLN
app.configure ->
    app.use express.methodOverride()
    app.use express.cookieParser()
    app.use express.session({secret: 'secret', key: 'express.sid'})
    app.use '/public', express.static(__dirname + '/../public')
    app.set 'views', __dirname+'/../views'
    app.engine 'html', consolidate.mustache
    app.set 'view engine', 'html'
    
app.get '/', (request, response) ->
    response.render 'index.html',
        title: 'indey'

app.get '/:tag', (request, response) ->
    response.render 'index.html',
        title: request.params.tag
        tag: request.params.tag

server.listen program.port, program.address, () ->
    console.log 'server is listening'


sio = io.listen server

sio.sockets.on 'connection', (socket) ->
    console.log 'client connected'

    socket.on 'data', (data) ->
        socket.broadcast.emit 'data:' + data.tag, data








