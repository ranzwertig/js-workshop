(function() {
  var app, consolidate, express, io, program, server, sio;

  program = require('commander');

  io = require('socket.io');

  express = require('express');

  app = express();

  server = (require('http')).createServer(app);

  consolidate = require('consolidate');

  program.option('-p, --port <n>', 'The port the programm is listening to', parseInt, 8080).option('-a, --address <n>', 'The Ip listenign to', '192.168.35.171').parse(process.argv);

  app.configure(function() {
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session({
      secret: 'secret',
      key: 'express.sid'
    }));
    app.use('/public', express["static"](__dirname + '/../public'));
    app.set('views', __dirname + '/../views');
    app.engine('html', consolidate.mustache);
    return app.set('view engine', 'html');
  });

  app.get('/', function(request, response) {
    return response.render('index.html', {
      title: 'indey'
    });
  });

  app.get('/:tag', function(request, response) {
    return response.render('index.html', {
      title: request.params.tag,
      tag: request.params.tag
    });
  });

  server.listen(program.port, program.address, function() {
    return console.log('server is listening');
  });

  sio = io.listen(server);

  sio.sockets.on('connection', function(socket) {
    console.log('client connected');
    return socket.on('data', function(data) {
      return socket.broadcast.emit('data:' + data.tag, data);
    });
  });

}).call(this);
