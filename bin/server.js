(function() {
  var http, server;

  http = require('http');

  server = http.createServer(function(req, res) {
    res.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    return res.end('Hello');
  });

  server.listen(8080, '127.0.0.1');

}).call(this);
