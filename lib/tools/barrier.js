(function() {
  'use strict';

  var Barrier,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  Barrier = (function() {

    function Barrier(parties, barrierCallback, abortCallback) {
      this.parties = parties;
      this.barrierCallback = barrierCallback;
      this.abortCallback = abortCallback != null ? abortCallback : function() {};
      this.skip = __bind(this.skip, this);

      this.abort = __bind(this.abort, this);

      this.submit = __bind(this.submit, this);

      this.running = true;
      this.count = 0;
    }

    Barrier.prototype.submit = function() {
      if (++this.count === this.parties && this.running) {
        return this.barrierCallback();
      }
    };

    Barrier.prototype.abort = function(customAbortCallback) {
      if (this.running && customAbortCallback) {
        customAbortCallback();
      } else if (this.running && this.abortCallback) {
        this.abortCallback();
      }
      return this.running = false;
    };

    Barrier.prototype.skip = function() {
      return this.barrierCallback();
    };

    return Barrier;

  })();

  module.exports = Barrier;

}).call(this);
