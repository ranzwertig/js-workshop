# WS_Bla

Nice project :)

## Getting Started
Install the module with: `npm install WS_Bla`

```javascript
var WS_Bla = require('WS_Bla');
WS_Bla.awesome(); // "awesome"
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_

## License
Copyright (c) 2013 Christian Ranz  
Licensed under the MIT license.
